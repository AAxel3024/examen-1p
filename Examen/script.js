const codigo = document.getElementById("codigo")
const marca = document.getElementById("marca")
const modelo = document.getElementById("modelo")
const year = document.getElementById("year")
const fecha_inicial = document.getElementById("fecha_inicial")
const fecha_final = document.getElementById("fecha_final")
const form = document.getElementById("form")
const parrafo = document.getElementById("warnings")
form.addEventListener("submit", e=>{
    e.preventDefault()
    let warnings = ""
    let entrar = false
    let vcodigo= /^[a-zA-Z0-9]{1,5}$/
    let vmarca=/^[a-zA-Z0-9]{1,50}$/
    let vmodelo=/^[a-zA-Z0-9]{1,30}$/
    let vyear=/^[0-9]{1,4}$/
    parrafo.innerHTML = ""
    if(codigo.value.length >5 || !vcodigo.test(codigo.value)){
        warnings += `Maximo 5 caracteres alfanumericos.<br>`
        entrar = true
    }
    if(marca.value.length >50 || !vmarca.test(marca.value)){
        warnings += `Maximo 50 caracteres alfanumericos.<br>`
        entrar = true
    }
    if(modelo.value.length >30 || !vmodelo.test(modelo.value)){
        warnings += `Maximo 30 caracteres alfanumericos.<br>`
        entrar = true
    }
    if(year.value.length >4 || !vyear.test(year.value)){
        warnings += `El año no es valido ingrese solo numeros.<br>`
        entrar = true
    }
    if(fecha_inicial.value>fecha_final.value){
        warnings += `fecha final mayor.<br>`
        entrar = true
    }
    if(entrar){
        parrafo.innerHTML = warnings
    }else{
        parrafo.innerHTML = "Enviado"
    }
})
